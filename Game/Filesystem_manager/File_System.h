#include <Windows.h>
#include <string>
#include <gl/GL.h>
#include <gl/GLU.h>

namespace fmng {
	
	class FileManager {
		
		std::string		m_vertex_shader_file_path;
		std::string		m_fragment_shader_file_path;
		std::string		m_vertex_shader_code;
		std::string		m_fragment_shader_code;
		
		FileManager();
		FileManager(const char* _vs_file, const char* _fs_file);
		FileManager(FileManager&) = delete;
		void operator=(FileManager&) {}
	public:
		
		const char* VertexShaderCode();
		const char* FragmentShaderCode();

		void SetShaderPath(const char* _vertex_shader_path, const char* _fragment_shader_path);
		bool GenerateShaderCode();
		bool ClearPath();

		static FileManager& get();
		static FileManager& get(const char* _vertex_shader_path, const char* _fragment_shader_path);
	};

}