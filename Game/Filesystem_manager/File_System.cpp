#include "File_System.h"
#include <fstream>
using fmng::FileManager;

//####################################### Constructos ####################################################

FileManager::FileManager() {
#ifndef STANDART_SHADER
	this->SetShaderPath("vs.glsl", "fs.glsl");
#endif 
}

FileManager::FileManager(const char* _vertex_shader_path, const char* _fragment_shader_path) {
	this->SetShaderPath(_vertex_shader_path, _fragment_shader_path);
}

//################################### SETTERS & GETTERS #################################################

void FileManager::SetShaderPath(const char* _vs_file, const char* _fs_file) {
	this->m_vertex_shader_file_path = _vs_file;
	this->m_fragment_shader_file_path = _fs_file;
}

const char* FileManager::VertexShaderCode() {
	return this->m_vertex_shader_code.c_str();
}

const char* FileManager::FragmentShaderCode() {
	return  this->m_fragment_shader_code.c_str();
}

//####################################### Instructions with shader #########################################

bool FileManager::GenerateShaderCode()  {
	size_t size;
	
	std::ifstream file(this->m_vertex_shader_file_path);
	file.seekg(0, std::ios::end);
	size = file.tellg();
	this->m_vertex_shader_code = new char[size];
	file.seekg(0);
	this->m_vertex_shader_code.reserve(size);
	file.read(&this->m_vertex_shader_code[0], size);
	file.close();

	file.open(this->m_fragment_shader_file_path);
	file.seekg(0, std::ios::end);
    size = file.tellg();
	this->m_fragment_shader_code = new char[size];
	file.seekg(0);
	this->m_fragment_shader_code.reserve(size);
	file.read(&this->m_fragment_shader_code[0], size);
	file.close();

	this->ClearPath();
	return 1;
}

bool FileManager::ClearPath() {
	this->m_fragment_shader_file_path.clear();
	this->m_vertex_shader_file_path.clear();
	return 1;
}

//####################################### GET #################################################

FileManager& FileManager::get() {
	FileManager* obj = new FileManager();
	return *obj;
}

FileManager& FileManager::get(const char* _vertex_shader_path, const char* _fragment_shader_path) {
	FileManager* obj = new FileManager(_vertex_shader_path, _fragment_shader_path);
	return *obj;
}