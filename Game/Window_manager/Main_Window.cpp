#include "Main_Window.h"
#include <cstring>
#include <string>
using wmng::MainWindow;

//####################################### Constructos ####################################################

MainWindow::MainWindow() :
	MainWindow("Main Window", 880, 495, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL)
{
}

MainWindow::MainWindow(const char* _title, const size_t& _w, const size_t & _h) :
	MainWindow(_title, _w, _h, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL)
{	
}

MainWindow::MainWindow(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y) :
	MainWindow(_title, _w, _h, _x, _y, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL)
{
}

MainWindow::MainWindow(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y, const Uint32& _flags) :
	m_window(nullptr), m_glcontext(NULL),
	m_window_height(_h), m_window_width(_w),
	m_window_x_position(_x), m_window_y_position(_y),
	m_window_flags(_flags)
{
	this->m_window_title = _title;
}
//##################################### StartUp & ShutDown ################################################

bool MainWindow::StartUp() {
	return this->InitSDL() && 
			this->InitGL() && 
			this->Window() && 
			this->GLContext();
}

bool MainWindow::ShutDown() {
	return 1;
}

//###################################### Initizialization ################################################

bool MainWindow::InitSDL() {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		std::string _error = "Unable to init SDL, error: ";
		_error += SDL_GetError();
		throw std::runtime_error(_error.c_str());
		return 0;
	}
	return 1;
}

bool MainWindow::InitGL()  {
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 6);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	glewInit();
	return 1;
}

bool MainWindow::Window() {
	this->m_window = SDL_CreateWindow(this->m_window_title.c_str(),
		this->m_window_x_position, this->m_window_y_position,
		this->m_window_width, this->m_window_height,
		this->m_window_flags);
	if (this->m_window == NULL) {
		std::string _error = "SDL window couldnt create: error: ";
		_error += SDL_GetError();
		throw std::runtime_error(_error.c_str());
		return 0;
	}
	return 1;
}

bool MainWindow::GLContext() {
	if (this->m_window == NULL) {
		throw std::runtime_error("GL context couldnt created");
		return 0;
	}	
	this->m_glcontext = SDL_GL_CreateContext(this->m_window);
	return 1;
}

//####################################### Return methods #################################################

size_t MainWindow::WindowHeight() const {
	return this->m_window_height;
}

size_t MainWindow::WindowWidth() const {
	return this->m_window_width;
}

size_t MainWindow::XPosition() const {
	return this->m_window_x_position;
}

size_t MainWindow::YPosition() const {
	return this->m_window_y_position;
}

const char* MainWindow::WindowTitle() const {
	return this->m_window_title.c_str();
}

//####################################### Rendering #################################################

void MainWindow::Display() {
	SDL_GL_SwapWindow(this->m_window);
}

void MainWindow::Render() {
	glFlush();
}

//####################################### GET #################################################

MainWindow& MainWindow::get()  {
	MainWindow* window = new MainWindow();
	return *window;
}

MainWindow& MainWindow::get(const char* _title, const size_t& _w, const size_t& _h)  {
	MainWindow* window = new MainWindow(_title, _w, _h);
	return *window;
}

MainWindow& MainWindow::get(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y) {
	MainWindow* window = new MainWindow(_title, _w, _h, _x, _y);
	return *window;
}

MainWindow& MainWindow::get(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y, const Uint32& _flags) {
	MainWindow* window = new MainWindow(_title, _w, _h, _x, _y, _flags);
	return *window;
}