#include <windows.h>
#include <SDL.h>
#include <glew.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdexcept>
#include <string>

namespace wmng {

	class MainWindow {
		MainWindow();
		MainWindow(const char* _title, const size_t& _w, const size_t& _h);
		MainWindow(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y);
		MainWindow(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y, const Uint32& _flags);
		MainWindow(MainWindow&) = delete;
		void operator=(MainWindow&) {}

		SDL_GLContext	m_glcontext;
		SDL_Window*		m_window;
		size_t			m_window_height;
		size_t			m_window_width;
		size_t			m_window_x_position;
		size_t			m_window_y_position;
		std::string			m_window_title;
		Uint32			m_window_flags;
	public:

		bool StartUp();
		bool ShutDown();

		bool InitSDL();
		bool InitGL();
		bool Window();
		bool GLContext();

		size_t WindowHeight() const;
		size_t WindowWidth() const;
		size_t XPosition() const;
		size_t YPosition() const;
		const char* WindowTitle() const;
	
		void Display();
		void Render();

		static MainWindow& get();
		static MainWindow& get(const char* _title, const size_t& _w, const size_t& _h);
		static MainWindow& get(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y);
		static MainWindow& get(const char* _title, const size_t& _w, const size_t& _h, const size_t& _x, const size_t& _y, const Uint32& _flasgs);

	};

}